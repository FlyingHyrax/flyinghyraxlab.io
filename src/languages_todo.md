---
title: Language Todo List
summary: A record of programming languages I want to learn or use more
layout: layouts/leaf.njk
eleventyNavigation:
  key: LanguageTodoList
  parent: Home
---

This is a rolling list of programming languages I want to learn. Many of them I have read about already to varying degrees, but haven't yet practiced or used for a complete piece of software.

So many languages, so little time!

> Presented in no particular order and with no claims regarding accuracy.

### Gleam

https://gleam.run/

- Functional
- Compiled
- Static typing
- Erlang's BEAM and Actors
- Comprehensive tooling a-la Go
- Inclusive community

### Janet

https://janet-lang.org

- Highly embeddable Lisp; project reminds me of Lua in a good way
- Almost no setup required, and good Windows support
- Can compile to standalone executables
- Standard library and runtime include a lot of stuff not found in similarly-tiny scripting languages:
  - Threads ("lightweight" / "green" threads)
  - an event loop
  - subprocess handling
  - networking
  - _Parsing expression grammars_

Repeat, PEGs are built right in to the language. Sweet.

_[Janet for Mortals](https://janet.guide) - a real book by Ian Henry_

### Zig

https://ziglang.org

- Competes w/ C ("robust, optimal, reusable")
- Explicit allocation / memory management
- Features focus on simplicity (no preprocessor, text macros, or hidden control flow)
- "Comptime" - compilation-time function execution and type manipulation
- ...zig compiler can compile C, so you can mix C/C++/Zig in one project
