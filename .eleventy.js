const yaml = require('js-yaml');
const navigationPlugin = require('@11ty/eleventy-navigation');
const syntaxPlugin = require('@11ty/eleventy-plugin-syntaxhighlight');
const dateFormatModule = import('dateformat');


let getMarkdownLib = function() {
    // set the markdown library instance to use so we can
    // change options and add plugins
    let markdownIt = require("markdown-it");

    // https://github.com/markdown-it/markdown-it#init-with-presets-and-options
    let options = {
        html: true,
        breaks: true,
        linkify: true
    };

    // load mathjax plugin:
    // https://www.npmjs.com/package/markdown-it-mathjax
    let mathjax = require('markdown-it-mathjax');

    // load header anchors plugin:
    // https://www.npmjs.com/package/markdown-it-anchor
    let anchors = require('markdown-it-anchor');

    // load pandoc-ish syntax extensions:
    let sups = require('markdown-it-sup');
    let subs = require('markdown-it-sub');
    let footnotes = require('markdown-it-footnote');

    return markdownIt(options)
        .use(mathjax())
        .use(subs)
        .use(sups)
        .use(footnotes)
        .use(anchors, {
            permalink: anchors.permalink.headerLink(),
        });
};

const uninterestingTags = new Set([
    "all", "post", "recent"
]);

module.exports = function(eleventyConfig) {

    // add syntax highlighting plugin; uses PrismJS
    eleventyConfig.addPlugin(syntaxPlugin);
    eleventyConfig.addPlugin(navigationPlugin);

    // customize markdown options
    eleventyConfig.setLibrary("md", getMarkdownLib());


    // load data files into the Data Cascade from YAML, not just JSON
    eleventyConfig.addDataExtension("yaml", contents => yaml.load(contents));

    // tell 11ty to copy contents of css folder to output without modification
    // (this path is relative to project root, not to input folder!)
    eleventyConfig.addPassthroughCopy("src/css");
    eleventyConfig.addPassthroughCopy("src/img");

    eleventyConfig.addFilter("dateSlug", function(value) {
        let y = value.getFullYear();
        let m = value.getMonth();
        let d = value.getDate();
        return `${y}/${m}/${d}`;
    });

    eleventyConfig.addAsyncFilter("dateFormat", async function(value, fmt) {
        const { default: dateFormat } = await dateFormatModule;
        return dateFormat(value, fmt);
    });

    // some of our collections are not meant as post categories or topics,
    // e.g. 'all', 'post', 'recent' and instead are used internally
    // to select subsets in a particular order on an index page or something. 
    eleventyConfig.addFilter("interestingTags", function(value) {
        const isInteresting = (k) => !uninterestingTags.has(k);
        if (Array.isArray(value)) {
            // support filtering a list of tags
            return value.filter(isInteresting);
        } else {
            // support filtering the "collections" object
            return Object.keys(value).filter(isInteresting);
        }
        
    });

    // Get N most recent posts
    // https://www.11ty.dev/docs/collections/#advanced-custom-filtering-and-sorting
    eleventyConfig.addCollection("recent", function(collectionApi) {
        return collectionApi.getFilteredByTag("post").filter((p) => !p.data.draft).sort((a, b) => b.date - a.date).slice(0, 3);
    });

    return {
        // input and output directories
        dir: {
            input: "src",
            output: "build"
        },
        // template formats to find and process
        templateFormats: ["html", "md", "njk"],
        // copy files that don't match a template format without modifying them
        passthroughFileCopy: true,
        // use nunjucks instead of liquid
        htmlTemplateEngine: "njk",
        markdownTemplateEngine: "njk"
    };
};
